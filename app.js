const canvas = document.getElementById('snake');
const context = canvas.getContext('2d');
const brickSize = 9;
const cellSize = 10;
const colsNumber = canvas.width / cellSize;
const rowsNumber = canvas.height / cellSize;
const possibleDirections = {37: 'left', 38: 'up', 39: 'right', 40: 'down'};

let speed = document.getElementById('speedToDisplay');
let contentMainText = document.getElementById('contentMainText');
let contentSubText = document.getElementById('contentSubText');

let score = document.getElementById('scoreToDisplay');
let currentScore = 0;
score.innerText = currentScore;

let randomColumnPosition = Math.floor(Math.random() * (rowsNumber - 2)) + 1;
let randomRowPosition = Math.floor(Math.random() * (rowsNumber - 2)) + 1;

let gameFinished = true;


function hideHtmlElement(htmlEl) {
    htmlEl.style.display = 'none';
}

function showHtmlElement(htmlEl) {
    htmlEl.style.display = 'block';
}

class Brick {
    constructor(rowPosition, columnPosition){
        this.rowPosition = rowPosition;
        this.columnPosition = columnPosition;
    };

    drawBrick(color) {
        let x = this.columnPosition * cellSize;
        let y = this.rowPosition * cellSize;
        context.fillStyle = color;
        context.fillRect(x, y, brickSize, brickSize);
    };

    collision(obj) {
        return this.columnPosition === obj.columnPosition && this.rowPosition === obj.rowPosition;
    };
}

class Food {
    constructor(){
        this.position = new Brick(randomRowPosition, randomColumnPosition);
    };

    drawFood() {
        this.position.drawBrick('#000');
    };

    generateRandomPosition() {
        randomRowPosition = Math.floor(Math.random() * (rowsNumber - 2)) + 1;
        randomColumnPosition = Math.floor(Math.random() * (colsNumber - 2)) + 1;
        this.position = new Brick(randomRowPosition, randomColumnPosition);
    };

}

let food = new Food();

class Snake {
    constructor(length) {
        this.length = length;
        this.body = [];
        this.nextDirection = 'left';
        this.currentDirection = 'left';
        this.currentSpeed = 1;
        this.speedLevelNames = ['worm', 'sloth', 'sonic', 'shazam', 'quicksilver', 'kid flash', 'flash'];

        for (let i = 0; i < this.length; i++) {
            this.body.push(new Brick(10, i + 10));
        }
    }

    drawBody() {
        for (let i = 0; i < this.body.length; i++) {
            this.body[i].drawBrick('#000');
        }
    };

    setMovingDirection(nextDirection) {
        if(
            ((nextDirection === 'left' || nextDirection === 'right') && (this.currentDirection === 'up' || this.currentDirection === 'down'))
            || ((nextDirection === 'up' || nextDirection === 'down') && (this.currentDirection === 'left' || this.currentDirection === 'right'))
        ) {
            this.nextDirection = nextDirection;
        }
    };

    checkCollision(head) {
        let leftWall = head.columnPosition < 0;
        let topWall = head.rowPosition < 0;
        let rightWall = head.columnPosition > colsNumber - 1;
        let downWall = head.rowPosition > rowsNumber - 1;

        let wallCollision = leftWall || topWall || rightWall || downWall;

        let selfCollision = false;
        for (let i = 0; i < this.body.length; i++) {
            if (head.collision(this.body[i])) {
                selfCollision = true;
            }
        }
        return wallCollision || selfCollision;
    };

    move() {
        speed.innerText = this.speedLevelNames[this.currentSpeed - 1];

        this.currentDirection = this.nextDirection;
        let head = this.body[0];
        let newHead;

        switch (this.currentDirection) {
            case possibleDirections[37]:
                newHead = new Brick(head.rowPosition, head.columnPosition - 1);
                break;
            case possibleDirections[38]:
                newHead = new Brick(head.rowPosition - 1, head.columnPosition);
                break;
            case possibleDirections[39]:
                newHead = new Brick(head.rowPosition, head.columnPosition + 1);
                break;
            case possibleDirections[40]:
                newHead = new Brick(head.rowPosition + 1, head.columnPosition);
                break;
        }

        if (this.checkCollision(newHead)) {
            gameOver();
            return;
        }

        this.body.unshift(newHead);

        if (newHead.collision(food.position)) {
            ++ currentScore;
            if (this.currentSpeed < this.speedLevelNames.length) {
                ++ this.currentSpeed;
            }
            score.innerText = currentScore;
            food.generateRandomPosition();
        } else {
            this.body.pop();
        }
    };
}

let snake = new Snake();

let gameStart = () => {
    gameFinished = false;
    snake = new Snake(4);
    snake.currentSpeed = 1;
    food = new Food();
    resetStats();
    hideHtmlElement(contentMainText);
    hideHtmlElement(contentSubText);
    startAnimation();

};

let startAnimation = () => {
    if(gameFinished) {
        return;
    }
    context.clearRect(0, 0, canvas.width, canvas.height);
    snake.move();
    snake.drawBody();
    food.drawFood();
    setTimeout(function () {
        window.requestAnimationFrame(startAnimation);
    }, 200 / snake.currentSpeed);
};


let gameOver = () => {
    gameFinished = true;
    showHtmlElement(contentMainText);
    showHtmlElement(contentSubText);
};

let resetStats = () => {
    currentScore = 0;
    score.innerText = currentScore;
};

document.addEventListener('keydown', keyControlHandler);
function keyControlHandler(e) {
    let keyCode = e.keyCode;
    if ((keyCode === 37 || keyCode === 38 || keyCode === 39 || keyCode === 40) && !gameFinished) {
            snake.setMovingDirection(possibleDirections[e.keyCode]);
    } else if (keyCode === 32 && gameFinished) {
        gameStart();
        gameFinished = false;
    }
}
